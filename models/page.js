var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    page: {
        type: String,
        required: true
    },
    pageSlug: {
        type: String,
        required: true,
        unique: true
    },
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    projectId: {
        type: Schema.Types.ObjectId,
        ref: 'Project',
        required: true
    }
});

module.exports = mongoose.model('Page', schema);
