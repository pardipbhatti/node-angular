var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var mongooseUniqueValidator = require('mongoose-unique-validator');

var schema = new Schema({
    gpPostTitle: {
        type: String,
        required: true
    },
    gpPostSlug: {
        type: String,
        required: true,
        unique: true
    },
    gpPostDescription: {
        type: String
    },
    gpPostPermalink: {
        type: String,
        required: true,
        unique: true
    },
    gpPostType: {
        type: String
    },
    gpPostDeleteStatus: {
        type: Number
    },
    gpPostCreatedOn: {
        type: String
    },
    gpPostUpdatedOn: {
        type: String
    }
});

schema.plugin(mongooseUniqueValidator);

module.exports = mongoose.model('Post', schema);
