var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    project: {
        type: String,
        required: true
    },
    projectSlug: {
        type: String,
        required: true,
        unique: true
    },
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    }
});

module.exports = mongoose.model('Project', schema);
