var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var mongooseUniqueValidator = require('mongoose-unique-validator');

var schema = new Schema({
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    phone: {
        type: Number
    },
    skype: {
        type: String
    },
    tagLine: {
        type: String
    },
    address: {
        type: String
    },
    facebook: {
        type: String
    },
    twitter: {
        type: String
    },
    google: {
        type: String
    },
    latitude: {
        type: String
    },
    longitude: {
        type: String
    },
    createdAt:{
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date
    }
})

schema.plugin(mongooseUniqueValidator);

module.exports = mongoose.model('User', schema);
