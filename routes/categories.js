var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
var gpGuard = require('./guard');

var Category = require('../models/categories');

/**
 * @Description: saving user
 * @Author: Pardip Bhatti (Gagudeep)
 */
router.post('/create',  function (req, res, next) {

    var user = new Category({
        gpCatName: req.body.gpCatName,
        gpCatSlug: req.body.gpCatSlug
    });

    user.save(function (err, cats) {
        if(err) {
            return res.json({
                title: 'errorOccoured',
                error: err
            });
        }

        res.json({
            'message': 'Category saved successfully',
            'cat': cats
        });
    });
});

/**
 * @Description: Authenticating user
 * @Author: Pardip Bhatti (Gagudeep)
 */
// router.put('/update/:id', function(req, res, next) {
//     User.update({_id: req.params.id}, req.body, function(err, response) {
//         if(err) {
//             res.json({
//                 title: 'errorOccured',
//                 error: err
//             })
//             return;
//         }
//
//         res.json({
//             title: 'success',
//             message: 'Profile updated successfully'
//         });
//     });
// });

/**
 * @Description: Authenticating user
 * @Author: Pardip Bhatti (Gagudeep)
 */
// router.get('/single/:id', function(req, res, next) {
//     User.findOne({ _id: req.params.id }, function (err, user) {
//         if(err) {
//             return res.json({
//                 title: 'errorOccured',
//                 error: err
//             });
//             return;
//         }
//
//         res.json(user);
//     });
// });

/**
 * @Description: Get all cats
 * @Author: Pardip Bhatti (Gagudeep)
 */
router.get('/all', function(req, res, next) {
    Category.find(function (err, cats) {
        if(err) {
            return res.json({
                title: 'errorOccured',
                error: err
            });
            return;
        }

        res.json({
            title: 'Categories found',
            cats: cats
        });
    });
});

/**
 * @Description: Get all cats
 * @Author: Pardip Bhatti (Gagudeep)
 */
router.delete('/remove/:id', function(req, res, next) {
    Category.remove({_id: req.params.id}, function (err) {
        if(err) {
            return res.json({
                title: 'errorOccured',
                error: err
            });
            return;
        }

        res.json({
            title: 'Category removed successfully'
        });
    });
});

module.exports = router;
