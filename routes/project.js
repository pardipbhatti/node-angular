var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');

var Project = require('../models/project');
var Page = require('../models/page');

router.post('/createProject', function (req, res, next) {
    console.log(req.body);
    var project = new Project({
        project: req.body.project,
        projectSlug: req.body.projectSlug,
        userId: req.body.userId

    });
    project.save(function(err, result) {
        if(err) {
            return res.status(500).json({
                title: 'An error occurred',
                error: err
            });
        }

        res.status(201).json({
            message: 'Project created successfully.',
            obj: result
        });
    });
});

router.get('/allProjects/:id', function (req, res, next) {

    Project.find({ userId: req.params.id }).exec(function (err, user) {
        if(err) {
            return res.status(500).json({
                title: 'An error occurred',
                error: err
            });
        }

        res.status(200).json({
           message: 'Project have data',
           userData: user
       });

    });
});

router.get('/deleteProjects/:id', function (req, res, next) {

    Project.remove({ _id: req.params.id }, function (err) {
        if(err) {
            return res.status(500).json({
                title: 'An error occurred',
                error: err
            });
        }

        Page.remove({projectId: req.params.id}, function(req, resProject, next){
            if(err) {
                return res.status(500).json({
                    title: 'An error occurred',
                    error: err
                });
            }

        });

        res.status(200).json({
            message: 'Project deleted'
        });


    });


});


router.get('/singleProject/:id', function (req, res, next) {

    Project.findOne({ _id: req.params.id }, function (err, project) {
        if(err) {
            return res.status(500).json({
                title: 'An error occurred',
                error: err
            });
        }

        res.status(200).json({
            message: 'Deleted',
            projectData: project
        });

    });


});


router.put('/updateProject/:id', function (req, res, next) {

    Project.update({ _id: req.params.id }, req.body, function (err) {
        if(err) {
            return res.status(500).json({
                title: 'An error occurred',
                error: err
            });
        }
        res.status(200).json({
           message: 'Project found'
        });
    });


});




module.exports = router;
