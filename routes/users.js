var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
var gpGuard = require('./guard');

var User = require('../models/user');

/**
 * @Description: saving user
 * @Author: Pardip Bhatti (Gagudeep)
 */
router.post('/',  function (req, res, next) {

  var user = new User({
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email,
      password: bcrypt.hashSync(req.body.password, 15)
  });

  user.save(function (err, user) {
      if(err) {
          return res.json({
              title: 'errorOccoured',
              error: err
          });
      }

      res.json({
          'message': 'User saved successfully',
          'user': user
      });
  });
});

/**
 * @Description: Authenticating user
 * @Author: Pardip Bhatti (Gagudeep)
 */
router.post('/login', function (req, res, next) {
   User.findOne({ email: req.body.email }, function (err, user) {
       if(err) {
           return res.json({
               title: 'errorOccured',
               error: { message: 'Invalid login credentials' }
           });
       }

       if(!user) {
           return res.json({
               title: 'errorOccured',
               error: { message: 'Invalid login credentials' }
           });
       }

       if(!bcrypt.compareSync(req.body.password, user.password)) {
           return res.json({
               title: 'errorOccured',
               error: { message: 'Invalid login credentials' }
           });
       }

       var token = jwt.sign({user: user}, 'Gagandeep@@9187', {expiresIn: 1000});
       res.json({
           message: 'Successfully logged in',
           token: token,
           userId: user._id
       });
   });
});

/**
 * @Description: Authenticating user
 * @Author: Pardip Bhatti (Gagudeep)
 */
router.put('/update/:id', function(req, res, next) {
    User.update({_id: req.params.id}, req.body, function(err, response) {
        if(err) {
            res.json({
                title: 'errorOccured',
                error: err
            })
            return;
        }

        res.json({
            title: 'success',
            message: 'Profile updated successfully'
        });
    });
});

/**
 * @Description: Authenticating user
 * @Author: Pardip Bhatti (Gagudeep)
 */
router.get('/single/:id', function(req, res, next) {
    User.findOne({ _id: req.params.id }, function (err, user) {
       if(err) {
           return res.json({
               title: 'errorOccured',
               error: err
           });
        return;
       }
       
       res.json(user);
    });
});

module.exports = router;
