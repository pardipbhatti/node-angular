var express = require('express');
var router = express.Router();
var gpGuard = require('./guard');

var Post = require('../models/post');

/**
 * @Description: Creating post
 * @Author: Pardip Bhatti (Gagudeep)
 */
router.post('/create',  function (req, res, next) {
    Post.findOne({gpPostSlug: req.body.gpPostSlug}, function(err, responseExist){
        if(err) {
            res.json({
                title: 'errorOccoured',
                error: err
            });
        }
        if(responseExist !== null) {
            if(responseExist.gpPostSlug === req.body.gpPostSlug) {   //@ Update post if already available
                var postData = {
                    gpPostTitle: req.body.gpPostTitle,
                    gpPostDescription: req.body.gpPostDescription,
                    gpPostPermalink: req.body.gpPostPermalink,
                    gpPostType: req.body.gpPostType,
                    gpPostDeleteStatus: req.body.gpPostDeleteStatus,
                    gpPostCreatedOn: req.body.gpPostCreatedOn
                }
                Post.update({gpPostSlug: req.body.gpPostSlug}, postData, function(err, postResponse){
                    if(err) {
                        res.json({
                            title: 'errorOccoured',
                            error: err
                        });
                    }
                    res.json({
                        message: 'Post updated successfully',
                        gpPostPermalink: req.body.gpPostPermalink,
                        gpPostSlug: req.body.gpPostSlug
                    });
                });

            }
        } else {
            // Create Post
            var post = new Post({
                gpPostTitle: req.body.gpPostTitle,
                gpPostSlug: req.body.gpPostSlug,
                gpPostDescription: req.body.gpPostDescription,
                gpPostPermalink: req.body.gpPostPermalink,
                gpPostType: req.body.gpPostType,
                gpPostDeleteStatus: req.body.gpPostDeleteStatus,
                gpPostCreatedOn: req.body.gpPostCreatedOn
            });
            post.save(function (err, post) {
                if(err) {
                    return res.json({
                        title: 'errorOccoured',
                        error: err
                    });
                }
                res.json({
                    message: 'Post saved successfully',
                    gpPostPermalink: post.gpPostPermalink
                });
            });
        }
    });
});

/**
 * @Description: Authenticating user
 * @Author: Pardip Bhatti (Gagudeep)
 */
// router.put('/update/:id', function(req, res, next) {
//     User.update({_id: req.params.id}, req.body, function(err, response) {
//         if(err) {
//             res.json({
//                 title: 'errorOccured',
//                 error: err
//             })
//             return;
//         }
//
//         res.json({
//             title: 'success',
//             message: 'Profile updated successfully'
//         });
//     });
// });

/**
 * @Description: Authenticating user
 * @Author: Pardip Bhatti (Gagudeep)
 */
// router.get('/single/:id', function(req, res, next) {
//     User.findOne({ _id: req.params.id }, function (err, user) {
//         if(err) {
//             return res.json({
//                 title: 'errorOccured',
//                 error: err
//             });
//             return;
//         }
//
//         res.json(user);
//     });
// });

/**
 * @Description: Get all cats
 * @Author: Pardip Bhatti (Gagudeep)
 */
// router.get('/all', function(req, res, next) {
//     Category.find(function (err, cats) {
//         if(err) {
//             return res.json({
//                 title: 'errorOccured',
//                 error: err
//             });
//             return;
//         }

//         res.json({
//             title: 'Categories found',
//             cats: cats
//         });
//     });
// });

/**
 * @Description: Get all cats
 * @Author: Pardip Bhatti (Gagudeep)
 */
// router.delete('/remove/:id', function(req, res, next) {
//     Category.remove({_id: req.params.id}, function (err) {
//         if(err) {
//             return res.json({
//                 title: 'errorOccured',
//                 error: err
//             });
//             return;
//         }

//         res.json({
//             title: 'Category removed successfully'
//         });
//     });
// });

module.exports = router;
